﻿using LibraryAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

namespace LibraryAPI.Services
{
    public class MailService
    {
       
        public void ChangeReservationStatus(Mail mail, Reservation reservation)
        {      
            switch (reservation.Status)
            {
                case "STARTED":
                    SendMail(mail, "[LIBRARYRR]Złożenie rezerwacji nr " + reservation.ReservationID, "Złożyłeś rezerwację na książkę <b>"+reservation.Book.Author+" - "+reservation.Book.Title+"</b><br>Książka oczekuje na Ciebie w naszej bibliotece przez 6 godzin! Masz czas do "+reservation.ReservationTime.AddHours(6));
                    break;
                case "PICKED_UP":
                    SendMail(mail, "[LIBRARYRR]Zmiana statusu rezerwacji nr " + reservation.ReservationID, "Zmieniono status rezerwacji nr <b>"+reservation.ReservationID+ "</b>, pozycja: <b>" + reservation.Book.Author + " - " + reservation.Book.Title + "</b> na <b><i>ODEBRANO</b></i>");
                    break;
                case "GAVE_BACK":           
                    SendMail(mail, "[LIBRARYRR]Zmiana statusu rezerwacji nr " + reservation.ReservationID, "Zmieniono status rezerwacji nr <b>" + reservation.ReservationID + "</b>, pozycja: <b>" + reservation.Book.Author + " - " + reservation.Book.Title + "</b> na <b><i>ZWRÓCONO</b></i><br><br>Dziękujemy za skorzystanie z usług naszej biblioteki!");          
                    break;
                case "CANCELED":            
                    SendMail(mail, "[LIBRARYRR]Zmiana statusu rezerwacji nr " + reservation.ReservationID, "Zmieniono status rezerwacji nr <b>" + reservation.ReservationID + "</b>, pozycja: <b>" + reservation.Book.Author + " - " + reservation.Book.Title + "</b> na <b><i>ANULOWANO</b></i><br><br>Zachęcamy do ponownego skorzystania z usług naszej biblioteki!");
                    break;
                default:
                    break;
            }          
        }

        public void CreateUser(Mail mail)
        {
            SendMail(mail, "Library RR - rejestracja", "Witaj <b>" + mail.Firstname + "</b>!<br>Dziękujemy za rejestracje w naszej bibliotece.<br><br><br>W razie jakichkolwiek pytań, nasz zespół jest dla Ciebie dostępny 24/7 <b>pomoc@libraryrr.pl</b>");  
        }

        private void SendMail(Mail mail, string title, string text)
        {
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("Library RR", "rafista.stanek@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(mail.Firstname+" "+mail.Surname, mail.Address));
            mailMessage.Subject = title;
            mailMessage.Body = new TextPart("html") { Text = text };

            using (var client = new SmtpClient())
            {
                client.Connect("in-v3.mailjet.com", 587, false);
                client.Authenticate("2a86a83e8ac08e21fbcc0a08722a8da2", "b1f2f8947b4cecf3c3deb9e748e8fa3b");
                client.Send(mailMessage);
                client.Disconnect(true);
            }

        }
    }
}
