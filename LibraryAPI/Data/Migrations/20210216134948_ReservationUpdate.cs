﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryAPI.Data.Migrations
{
    public partial class ReservationUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reservations_Books_BookID",
                table: "Reservations");

            migrationBuilder.DropIndex(
                name: "IX_Reservations_BookID",
                table: "Reservations");

            migrationBuilder.AddColumn<int>(
                name: "ReservationID",
                table: "Books",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Books_ReservationID",
                table: "Books",
                column: "ReservationID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Books_Reservations_ReservationID",
                table: "Books",
                column: "ReservationID",
                principalTable: "Reservations",
                principalColumn: "ReservationID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_Reservations_ReservationID",
                table: "Books");

            migrationBuilder.DropIndex(
                name: "IX_Books_ReservationID",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "ReservationID",
                table: "Books");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_BookID",
                table: "Reservations",
                column: "BookID");

            migrationBuilder.AddForeignKey(
                name: "FK_Reservations_Books_BookID",
                table: "Reservations",
                column: "BookID",
                principalTable: "Books",
                principalColumn: "BookID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
