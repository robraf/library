﻿using LibraryAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
 DB update command: Add-migration [name] -o Data/Migrations
Update-Database
 */

namespace LibraryAPI.Data
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Reservation>()
                .HasOne(p => p.Book)
                .WithOne(b => b.Reservation)
                .HasForeignKey<Book>(c => c.ReservationID)
                .OnDelete(DeleteBehavior.ClientSetNull);

            base.OnModelCreating(builder);

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
    }
}
