﻿using LibraryAPI.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Data
{
    public class FakeData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<LibraryContext>();
                context.Database.EnsureCreated();

                if (context.Users != null && context.Users.Any())
                    return;   // DB has already been seeded

                if (context.Books != null && context.Books.Any())
                    return;   // DB has already been seeded

                var users = GetUsers().ToArray();
                context.Users.AddRange(users);
                context.SaveChanges();

                var books = GetBooks().ToArray();
                context.Books.AddRange(books);
                context.SaveChanges(); 
            }
        }

        public static List<User> GetUsers()
        {
            List<User> users = new List<User>() {
                new User {Login="user", Password="user", Role="user", Firstname="user", Surname="user"},
                new User {Login="admin", Password="admin", Role="admin", Firstname="admin", Surname="admin"},
                new User {Login="employee", Password="employee", Role="employee", Firstname="employee", Surname="employee"},
            };
            return users;
        }

        public static List<Book> GetBooks()
        {
            List<Book> books = new List<Book>() {
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
                new Book {Title="Trzynasta opowieść", Author="Diane Setterfield", Publisher="Albatros", PublishedYear=2021, Language="pl",Description="Okładka twarda, strona 478", Category = "powieść",Location="A1", ReservationID=null},
                new Book {Title="Wszyscy muszą zginąć", Author="Marcel Moss", Publisher="Filia", PublishedYear=2021, Language="pl",Description="Okładka miękka, stron 352", Category = "kryminał",Location="C1", ReservationID=null},
            };
            return books;
        }
    }
}
