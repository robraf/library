﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }

        public String Login { get; set; }

        public String Password { get; set; }

        public String Firstname { get; set; }

        public String Surname { get; set; }

        public String Role { get; set; }

        public DateTime RegisterDateTime { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
