﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{
    public class Mail
    {
        public String Address { get; set; }
        public String Firstname { get; set; }
        public String Surname { get; set; }
        public int Id { get; set; }


        public Mail(string address, string firstname, string surname, int id)
        {
            Address = address;
            Firstname = firstname;
            Surname = surname;
            Id = id;
        } 
    }
}
