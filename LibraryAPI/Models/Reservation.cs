﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{

    public enum ReservationStatus
    {
        STARTED,
        PICKED_UP,
        GAVE_BACK,
        FINISHED,
        CANCELED
    }
    public class Reservation
    {
        [Key]
        public int ReservationID { get; set; }
     
        public int UserID { get; set; }

        public User User { get; set; }

        [ForeignKey("BookID")]
        public int? BookID { get; set; }

        public Book Book { get; set; }

        public String Status { get; set; }

        public DateTime ReservationTime { get; set; }

        public DateTime ReservationPickUpTime { get; set; }

        public DateTime ReservationEndTime { get; set; }
    }
}
