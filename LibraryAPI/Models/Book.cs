﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Models
{
    public class Book
    {
        [Key]
        public int BookID { get; set; }

        public String Title { get; set; }

        public String Author { get; set; }

        public String Publisher { get; set; }

        public int PublishedYear { get; set; }

        public String Description { get; set; }

        public String Language { get; set; }

        public String Category { get; set; }

        public String Location { get; set; }

        public Reservation Reservation { get; set; }

        [ForeignKey("ReservationID")]
        public int? ReservationID { get; set; }
    }
}
