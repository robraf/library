﻿using LibraryAPI.Data;
using LibraryAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LibraryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("LibraryPolicy")]
    [Authorize]
    public class BookController : ControllerBase
    {
        private readonly LibraryContext libraryContext;


        public BookController(LibraryContext context)
        {
            libraryContext = context;
        }

        // GET: api/book
        [HttpGet]
        public IEnumerable<Book> GetBooks()
        {
            return libraryContext.Books.Include(a => a.Reservation).Include(b => b.Reservation.User);
        }

        [HttpGet("findById")]
        public async Task<IActionResult> GetBookById([FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var book = await libraryContext.Books
                .Include(a => a.Reservation)
                .Include(b => b.Reservation.User)
                .FirstOrDefaultAsync(i => i.BookID == id);

            if (book == null)
            {
                return NoContent(); // NotFound();

            }
            return Ok(book);
        }

        // GET api/book/findByAuthor?author=JanekStanek
        [HttpGet("findByAuthor")]
        public async Task<IActionResult> GetBooksByAuthor([FromQuery] string author)
        {
            if (!ModelState.IsValid || author == null)
            {
                return BadRequest(ModelState);
            }
           
            var books = libraryContext.Books
                .Include(a => a.Reservation)
                .Include(b => b.Reservation.User)
                .Where(m => m.Author == author);

            return Ok(books);
        }

        // GET api/book/findByPublisher?publisher=Nowa Era
        [HttpGet("findByPublisher")]
        public async Task<IActionResult> GetBooksByPublisher([FromQuery] string publisher)
        {
            if (!ModelState.IsValid || publisher == null)
            {
                return BadRequest(ModelState);
            }
            var books = libraryContext.Books
               .Include(a => a.Reservation)
               .Include(b => b.Reservation.User)
               .Where(m => m.Publisher == publisher);

            return Ok(books);
        }

        // GET api/book/findByLanguage?language=pl
        [HttpGet("findByLanguage")]
        public async Task<IActionResult> GetBooksByLanguage([FromQuery] string language)
        {
            if (!ModelState.IsValid || language == null)
            {
                return BadRequest(ModelState);
            }
            var books = libraryContext.Books
               .Include(a => a.Reservation)
               .Include(b => b.Reservation.User)
               .Where(m => m.Language == language);

            return Ok(books);
        }

        // GET api/book/findByCategory?category=kryminał
        [HttpGet("findByCategory")]
        public async Task<IActionResult> GetBooksByCategory([FromQuery] string category)
        {
            if (!ModelState.IsValid || category == null)
            {
                return BadRequest(ModelState);
            }
            var books = libraryContext.Books
               .Include(a => a.Reservation)
               .Include(b => b.Reservation.User)
               .Where(m => m.Category == category);

            return Ok(books);
        }

        [HttpPost]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> PostBook([FromBody] Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(book.Title!=null && book.Author!=null))
            {
                return BadRequest(ModelState);
            }
            else
            {         
                libraryContext.Books.Add(book);
                await libraryContext.SaveChangesAsync();
                return CreatedAtAction("GetBookById", book);
            }
        }

        // DELETE api/book?id=1
        [HttpDelete]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> DeleteBook([FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var book = await libraryContext.Books.FindAsync(id);

            if (book == null)
            {
                return NoContent(); // NotFound();

            }
            libraryContext.Books.Remove(book);
            await libraryContext.SaveChangesAsync();
            return Ok(book);
        }

        // PUT api/book?id=1
        [HttpPut]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> EditBook([FromQuery] int id, [FromBody] Book book)
        {
            if (!ModelState.IsValid || id == null)
            {
                return BadRequest(ModelState);
            }

            var currentBook = await libraryContext.Books.FindAsync(id);

            if (currentBook == null)
                return NoContent();

            if (id != book.BookID)
            {
                return BadRequest();
            }

            if (book.Author != null)
            {
                currentBook.Author = book.Author;
            }

            if (book.Title != null)
            {
                currentBook.Title = book.Title;
            }

            if (book.Language != null)
            {
                currentBook.Language = book.Language;
            }

            if (book.Description != null)
            {
                currentBook.Description = book.Description;
            }

            if (book.Category != null)
            {
                currentBook.Category = book.Category;
            }

            if (book.Location != null)
            {
                currentBook.Location = book.Location;
            }

            if (book.PublishedYear != 0)
            {
                currentBook.PublishedYear = book.PublishedYear;
            }

            if (book.Publisher != null)
            {
                currentBook.Publisher = book.Publisher;
            }

            await libraryContext.SaveChangesAsync();
            return Ok(book);
        }

    }
}
