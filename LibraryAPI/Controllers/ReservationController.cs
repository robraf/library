﻿using LibraryAPI.Data;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("LibraryPolicy")]
    public class ReservationController : ControllerBase
    {
        private readonly LibraryContext libraryContext;

        public ReservationController(LibraryContext context)
        {
            libraryContext = context;
        }

        // GET: api/reservation
        [HttpGet]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> GetReservations()
        {
            return Ok(libraryContext.Reservations.Include(a => a.User).Join(libraryContext.Books, reservation => reservation.BookID, book => book.BookID,
                (reservation, book) => new
                {
                    reservationId = reservation.ReservationID,
                    user = reservation.User,
                    book,
                    reservation

                }));
            /*return Ok(libraryContext.Reservations.Include(a => a.User).Include(b => b.Book));*/

        }

        // GET: api/reservation/findByUser?id=
        [HttpGet("findByUser")]
        public async Task<IActionResult> GetReservationsByUser([FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //var users = libraryContext.Reservations.Include(a => a.User).Where(c => c.UserID == id).Include(b => b.Book);
            var users = libraryContext.Reservations.Include(a => a.User).Where(c => c.UserID == id).Join(libraryContext.Books, reservation => reservation.BookID, book => book.BookID,
                (reservation, book) => new
                {
                    reservationId = reservation.ReservationID,
                    user = reservation.User,
                    book,
                    reservation

                });
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> PostReservation([FromBody] Reservation reservation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(reservation.BookID != 0 && reservation.UserID != 0))
            {
                return BadRequest(ModelState);
            }
            else
            {
                //check is User exist
                var userExist = await libraryContext.Users.FirstOrDefaultAsync(i => i.UserID == reservation.UserID);
                var bookExist = await libraryContext.Books.Include(b => b.Reservation).FirstOrDefaultAsync(i => i.BookID == reservation.BookID);
                
                if (userExist != null)
                {
                    if (bookExist != null)
                    {
                        if (bookExist.Reservation == null)
                        {
                            reservation.Book = bookExist;
                            reservation.User = userExist;
                            reservation.Status = ReservationStatus.STARTED.ToString();
                            reservation.ReservationTime = DateTime.Now;
                       
                            var entry = libraryContext.Entry(bookExist);
                            bookExist.Reservation = reservation;
                            bookExist.ReservationID = reservation.ReservationID;
                            entry.OriginalValues.SetValues(bookExist);
                            entry.CurrentValues.SetValues(bookExist);

                            libraryContext.Reservations.Add(reservation);
                            await libraryContext.SaveChangesAsync();
                            new MailService().ChangeReservationStatus(new Mail(userExist.Login, userExist.Firstname, userExist.Surname, userExist.UserID), reservation);
                            return Ok(reservation);
                        }
                        else
                        {
                            return BadRequest(new { type = 2 }); //book is taken
                        }                  
                    }
                    else
                    {
                        return BadRequest(new { type = 1 }); //book doesn't exsist
                    }
                }
                else
                {
                    return BadRequest(new { type = 0 }); //user doesn't exsist
                }
            }
        }

        // DELETE api/reservation?id=1
        [HttpDelete]
        public async Task<IActionResult> DeleteReservation([FromQuery] int id)
        {       
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reservation = await libraryContext.Reservations.FindAsync(id);

            if (reservation == null)
            {
                return NoContent(); // NotFound();
            }
            else
            {               
                libraryContext.Reservations.Remove(reservation);
                var book = await libraryContext.Books.FindAsync(reservation.BookID);
                book.Reservation = null;
                book.ReservationID = null;
                libraryContext.Entry(book).CurrentValues.SetValues(book);
                await libraryContext.SaveChangesAsync();
                return Ok(new { status = "deleted"});
            }         
        }

        // PUT api/reservation/0?id=1
        [HttpPut("{status}")]    
        public async Task<IActionResult> EditBook([FromQuery] int id, [FromRoute] int status)
        {
            if (!ModelState.IsValid || id == null || !(status >=0 && status <=4))
            {
                return BadRequest(ModelState);
            }

            //var reservation = await libraryContext.Reservations.FindAsync(id);
            var reservation = await libraryContext.Reservations.Include(a => a.User).Include(b => b.Book).SingleOrDefaultAsync(c => c.ReservationID == id);

            if (reservation == null)
                return NoContent();

            var book = await libraryContext.Books.FindAsync(reservation.BookID);

            if (reservation.Status == "FINISHED" || reservation.Status == "CANCELED")
            {
                return BadRequest(new { type = 0 }); //reservation done
            }
            
            string reservationStatus = "";
            switch (status)
            {
                case 0:
                    reservationStatus = ReservationStatus.STARTED.ToString();
                    break;
                case 1:           
                    reservationStatus = ReservationStatus.PICKED_UP.ToString();
                    reservation.ReservationPickUpTime = DateTime.Now;                  
                    break;
                case 2:
                    reservationStatus = ReservationStatus.GAVE_BACK.ToString();
                    reservation.ReservationEndTime = DateTime.Now;
                    break;
                case 3:
                    reservationStatus = ReservationStatus.FINISHED.ToString();              
                    book.ReservationID = null;
                    book.Reservation = null;
                    libraryContext.Entry(book).CurrentValues.SetValues(book);
                    break;
                case 4:
                    reservationStatus = ReservationStatus.CANCELED.ToString();                
                    book.ReservationID = null;
                    book.Reservation = null;
                    libraryContext.Entry(book).CurrentValues.SetValues(book);
                    break;
                default:
                    break;
            }
            reservation.Status = reservationStatus;
            libraryContext.Entry(reservation).CurrentValues.SetValues(reservation);
            await libraryContext.SaveChangesAsync();
            if (reservation.User != null && reservation.Book != null)
            {
                new MailService().ChangeReservationStatus(new Mail(reservation.User.Login, reservation.User.Firstname, reservation.User.Surname, reservation.User.UserID), reservation);
            }
            return Ok(reservation);
        }

    }
}
