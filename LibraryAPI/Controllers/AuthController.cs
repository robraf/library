﻿using LibraryAPI.Data;
using LibraryAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("LibraryPolicy")]
    public class AuthController : ControllerBase
    {
        private readonly LibraryContext libraryContext;

        public AuthController(LibraryContext context)
        {
            libraryContext = context;
        }

        // POST: api/auth/token
        [HttpPost("token")]
        public async Task<IActionResult> GetToken([FromBody] Auth auth)
        {
            if (!ModelState.IsValid || auth.Login==null || auth.Password == null)
            {
                return BadRequest(ModelState);
            }

            //security key
            string securityKey = "w16HLJ5P54L9YrF0dTyFTc5XxRJurvlS";
            //synmetric security key
            var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            //credentials signing
            var signingCredentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            var user = libraryContext.Users.FirstOrDefault(i => i.Login == auth.Login);

            if (user != null)
            {
                if(user.Password == auth.Password)
                {
                    //add claims
                    var claims = new List<Claim>();
                    switch (user.Role)
                    {
                        case "user":
                            claims.Add(new Claim(ClaimTypes.Role, "user"));
                            break;
                        case "admin":
                            claims.Add(new Claim(ClaimTypes.Role, "admin"));
                            break;
                        case "employee":
                            claims.Add(new Claim(ClaimTypes.Role, "employee"));
                            break;
                        default:
                            break;
                    }
                    

                    //create token
                    var token = new JwtSecurityToken(
                        issuer: "smesk.in",
                        audience: "readers",
                        expires: DateTime.Now.AddHours(1),
                        signingCredentials: signingCredentials, claims: claims
                        );

                    //return token
                    return Ok(new { userID = user.UserID, role = user.Role, token = new JwtSecurityTokenHandler().WriteToken(token) });
                }
                else
                {
                    return BadRequest(new { type = 1 }); //incorrect password
                }        
            }
            else
            {
                return BadRequest(new { type = 2 }); //used doesn't exist
            }

           
        }

        
    }
}
