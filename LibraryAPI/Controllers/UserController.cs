﻿using LibraryAPI.Data;
using LibraryAPI.Models;
using LibraryAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LibraryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("LibraryPolicy")]
    public class UserController : ControllerBase
    {
        private readonly LibraryContext libraryContext;


        public UserController(LibraryContext context)
        {
            libraryContext = context;
        }

        // GET: api/user
        [HttpGet]
        [Authorize(Roles = "admin,employee")]
        public IEnumerable<User> GetUsers()
        {
            return libraryContext.Users;
        }

        // GET api/user/findById?id=1
        [HttpGet("findById")]
        [Authorize(Roles = "user,admin,employee")]
        public async Task<IActionResult> GetUserById([FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var user = libraryContext.Users.FirstOrDefault(i => i.UserID == id);
            var user = await libraryContext.Users.FindAsync(id);
            if (user == null)
            {
                return NoContent(); // NotFound();
                
            }
            return Ok(user);
        }

        // GET api/user/findByLogin?login=rafista
        [HttpGet("findByLogin")]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> GetUserByLogin([FromQuery] string login)
        {
           if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await libraryContext.Users.FirstOrDefaultAsync(i => i.Login == login);
            //var user = await libraryContext.Users.FindAsync();
            if (user == null)
            {
                return NoContent(); // NotFound();

            }
            return Ok(user);
        }

        // GET api/user/findByRole?role=user
        [HttpGet("findByRole")]
        [Authorize(Roles = "admin,employee")]
        public async Task<IActionResult> GetUserByRole([FromQuery] string role)
        {
            if (!ModelState.IsValid || role==null)
            {
                return BadRequest(ModelState);
            }
            var users = libraryContext.Users.Where(m => m.Role == role);
            
            return Ok(users);       
        }

        // POST api/user
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(!(user.Login != null && user.Password != null && user.Firstname != null && user.Surname != null))
            {
                return BadRequest(ModelState);
            }
            else
            {
                user.RegisterDateTime = DateTime.Now;

                //check is login exist
                var userExistByLogin = await libraryContext.Users.FirstOrDefaultAsync(i => i.Login == user.Login);

                if (userExistByLogin == null)
                {
                    user.Role = "user";
                    libraryContext.Users.Add(user);
                    await libraryContext.SaveChangesAsync();               
                    new MailService().CreateUser(new Mail(user.Login, user.Firstname, user.Surname, user.UserID));
                    return CreatedAtAction("GetUserByLogin", new { id = user.UserID, login = user.Login, firstname = user.Firstname, surname = user.Surname, role = user.Role, registerDateTime = user.RegisterDateTime }, user);
                }
                else
                {
                    return BadRequest(new { type = 0 }); //login is taken
                }
            }
        }

        // POST api/user/employee
        [HttpPost("employee")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PostEmployee([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!(user.Login != null && user.Password != null && user.Firstname != null && user.Surname != null))
            {
                return BadRequest(ModelState);
            }
            else
            {
                user.RegisterDateTime = DateTime.Now;

                //check is login exist
                var userExistByLogin = await libraryContext.Users.FirstOrDefaultAsync(i => i.Login == user.Login);

                if (userExistByLogin == null)
                {
                    user.Role = "employee";
                    libraryContext.Users.Add(user);
                    await libraryContext.SaveChangesAsync();
                    return CreatedAtAction("GetUserByLogin", new { id = user.UserID, login = user.Login, firstname = user.Firstname, surname = user.Surname, role = user.Role, registerDateTime = user.RegisterDateTime }, user);
                }
                else
                {
                    return BadRequest(new { type = 0 }); //login is taken
                }
            }
        }

        // PUT api/user?id=1
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> EditUser([FromQuery] int id, [FromBody] User user)
        {
            if (!ModelState.IsValid || id==null)
            {
                return BadRequest(ModelState);
            }

            var currentUser = await libraryContext.Users.FirstOrDefaultAsync(i => i.UserID == id);
        
            if (currentUser == null)
                return NoContent();

            if (id != user.UserID)
            {
                return BadRequest();
            }

            if (user.Login != null)
            {
                currentUser.Login = user.Login;
            }
            if (user.Password != null)
            {
                currentUser.Password = user.Password;
            }
            if (user.Firstname != null)
            {
                currentUser.Firstname = user.Firstname;
            }
            if (user.Surname != null)
            {
                currentUser.Surname = user.Surname;
            }
            if (user.Role != null)
            {
                currentUser.Role = user.Role;
            }
            if (user.RegisterDateTime != null)
            {
                currentUser.RegisterDateTime = user.RegisterDateTime;
            }

            await libraryContext.SaveChangesAsync();
            return Ok(user);
        }

        // DELETE api/user?id=1
        [HttpDelete]
        [Authorize(Roles = "admin,employee")]   
        public async Task<IActionResult> DeleteUser([FromQuery]int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await libraryContext.Users.FindAsync(id);

            if (user == null)
            {
                return NoContent(); // NotFound();

            }
            libraryContext.Users.Remove(user);
            await libraryContext.SaveChangesAsync();
            return Ok(user);
        }
    }
}
