import React from 'react';
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';

import Header from './components/Header';
import Footer from './components/Footer';
import Router from './components/Router';

const saltedMd5 = require('salted-md5');
require('dotenv/config');

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = { user: { userID: null, role: null, token: null }, emailError: null, passwordError: null, redirectToLogin: false };
		this.login = this.login.bind(this);
		this.register = this.register.bind(this);
		this.logout = this.logout.bind(this);
		this.saveStateToLocalStorage = this.saveStateToLocalStorage.bind(this);
	}

	async login(email, password) {
		if (email !== '' && password !== '') {
			const hashPass = await saltedMd5(password, process.env.REACT_APP_PASSWORD_HASH, true);
			await axios.post(`${process.env.REACT_APP_API_URL}api/auth/token`, {
				headers: {
					'Content-Type': 'application/json'
				},
				login: email,
				password: hashPass
			}).then(res => {
				this.setState({
					user: {
						userID: res.data.userID,
						role: res.data.role,
						token: res.data.token
					},
					emailError: null,
					passwordError: null
				});
			}).catch(error => {
				if (error.response.status === 400) {
					switch (error.response.data.type) {
						case 1:
							this.setState({ passwordError: 'Niepoprawne hasło.', emailError: null });
							break;
						case 2:
							this.setState({ emailError: 'Użytkownik nie istnieje.', passwordError: null });
							break;
						default:
							console.log(error.response);
							break;
					}
				}
			});
		}
	}

	async register(firstname, lastname, email, password, password2) {
		if (firstname !== '' && lastname !== '' && email !== '' && password !== '' && password2 !== '') {
			const hashPass = await saltedMd5(password, process.env.REACT_APP_PASSWORD_HASH, true);
			await axios.post(`${process.env.REACT_APP_API_URL}api/user`, {
				headers: {
					'Content-Type': 'application/json'
				},
				firstname: firstname,
				surname: lastname,
				login: email,
				password: hashPass
			}).then(() => {
				alert('Pomyślnie zarejestrowano. Możesz się teraz zalogować.');
				this.setState({ emailError: null });
				this.setState({ redirectToLogin: true });
			}).catch(error => {
				if (error.response.status === 400) {
					switch (error.response.data.type) {
						case 0:
							this.setState({ emailError: 'Istnieje już konto na tym adresie email.' });
							break;
						default:
							console.log(error.response);
							break;
					}
				}
			});
		}
	}

	async logout() {
		localStorage.clear();
		this.setState({
			user: {
				userID: null,
				role: null,
				token: null
			}
		});
	}

	saveStateToLocalStorage() {
		for (let key in this.state.user) {
			localStorage.setItem(key, JSON.stringify(this.state.user[key]));
		}
	}

	componentDidMount() {
		this.setState({
			user: {
				userID: JSON.parse(localStorage.getItem('userID')),
				role: JSON.parse(localStorage.getItem('role')),
				token: JSON.parse(localStorage.getItem('token'))
			}
		});
		localStorage.clear();
		window.addEventListener('beforeunload', this.saveStateToLocalStorage);
	}

	componentWillUnmount() {
		window.removeEventListener('beforeunload', this.saveStateToLocalStorage);
		this.saveStateToLocalStorage();
	}

	customStyle = {
		zIndex: '100'
	};

	render() {
		var user = null;
		if (this.state.user?.userID) {
			user = { userID: this.state.user.userID, role: this.state.user.role, token: this.state.user.token };
		}
		const emailError = this.state.emailError;
		const passwordError = this.state.passwordError;
		const redirectToLogin = this.state.redirectToLogin;

		return (
			<div>
				<Row>
					<Col><Header user={user} /></Col>
				</Row>
				<Row className='container'>
					<Col className='pr-0'>
						<Router
							user={user}
							login={this.login}
							emailError={emailError}
							passwordError={passwordError}
							register={this.register}
							logout={this.logout}
							redirectToLogin={redirectToLogin}
						/>
					</Col>
				</Row>
				<Row>
					<Col><Footer /></Col>
				</Row>
			</div>
		);
	}
}

export default App;
