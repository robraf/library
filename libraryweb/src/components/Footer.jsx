import React from 'react';

const Footer = () => (
	<footer className='py-2 bg-dark text-center'>
		<small className='text-white'>
			Copyright 2021 © 2RStudio
			</small>
	</footer>
)

export default Footer;