import React from 'react';

const NotFound = () => (
	<h2 className='mt-2'>
		Sorry, page not found :(
	</h2>
)

export default NotFound;