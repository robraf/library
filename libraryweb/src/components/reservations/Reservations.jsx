import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Select from 'react-select';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Button, Modal, Spinner } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function Reservations(props) {
	const user = props.user;
	const [reservations, setReservations] = useState([]);
	const [loading, setLoading] = useState(false);
	const [clickedID, setClickedID] = useState(null);
	const [showCancelModal, setShowCancelModal] = useState(false);
	const [showStatusModal, setShowStatusModal] = useState(false);
	const [status, setStatus] = useState(null);
	const [changingStatus, setChangingStatus] = useState(false);
	const { SearchBar, ClearSearchButton } = Search;

	const handleShowCancelModal = () => setShowCancelModal(true);
	const handleCloseCancelModal = () => setShowCancelModal(false);
	const handleShowStatusModal = () => setShowStatusModal(true);
	const handleCloseStatusModal = () => setShowStatusModal(false);
	const history = useHistory();

	const changeStatus = async () => {
		var stat = null;
		console.log(status);
		if (status === null) {
			stat = selectOptions[0].value;
		} else {
			stat = parseInt(status.value);
		}
		console.log(stat, clickedID, user.token);
		try {
			setChangingStatus(true);
			await axios.put(`${process.env.REACT_APP_API_URL}api/reservation/${stat}?id=${clickedID}`, {}, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(() => {
				setChangingStatus(false);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					setChangingStatus(false);
					console.log(error);
				}
			});
		} catch (e) {
			console.log(e);
		}
		handleCloseStatusModal();
		history.push('/reload');
		history.goBack();
	};

	const cancelReservation = async () => {
		try {
			await axios.delete(`${process.env.REACT_APP_API_URL}api/reservation?id=${clickedID}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			handleCloseCancelModal();
		} catch (e) {
			console.log(e);
		}
		history.push('/reload');
		history.goBack();
	};

	const getReservations = async () => {
		if (user.role !== 'user') {
			try {
				await axios.get(`${process.env.REACT_APP_API_URL}api/reservation`, {
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${user.token}`
					}
				}).then(res => {
					const reservations = [];
					for (let reservation of res.data) {
						if (reservation.reservation.status === 'STARTED' || reservation.reservation.status === 'PICKED_UP') {
							reservations.push(reservation);
						}
					}
					setReservations(reservations);
				}).catch(error => {
					if (error.response.status === 401) {
						props.logout();
					} else {
						console.log(error);
					}
				});
				setLoading(true);
			} catch (e) {
				console.log(e);
			}
		} else {
			try {
				await axios.get(`${process.env.REACT_APP_API_URL}api/reservation/findByUser?id=${user.userID}`, {
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${user.token}`
					}
				}).then(res => {
					const reservations = [];
					for (let reservation of res.data) {
						if (reservation.reservation.status === 'STARTED' || reservation.reservation.status === 'PICKED_UP') {
							let newReservation = {
								reservationID: reservation.reservationID
							};
							reservations.push(reservation);
						}
					}
					setReservations(reservations);
				}).catch(error => {
					if (error.response.status === 401) {
						props.logout();
					} else {
						console.log(error);
					}
				});
				setLoading(true);
			} catch (e) {
				console.log(e);
			}
		}
	};

	const columns = [{
		dataField: 'reservation.reservationID',
		text: 'ID',
		hidden: true
	}, {
		dataField: 'user.userID',
		text: 'Użytkownik',
		formatter: (column, row) => (`${row.user.firstname} ${row.user.surname}`),
		sort: true,
		hidden: user.role !== 'user' ? false : true
	}, {
		dataField: 'book.bookID',
		text: 'Książka',
		formatter: (column, row) => (row.book.title),
		sort: true
	}, {
		dataField: 'reservation.reservationTime',
		text: 'Data rezerwacji',
		formatter: (date) => (
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.reservationPickUpTime',
		text: 'Data odbioru',
		formatter: (date) => (date === '0001-01-01T00:00:00' ?
			'Brak daty odbioru' :
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.reservationEndTime',
		text: 'Data oddania',
		formatter: (date) => (date === '0001-01-01T00:00:00' ?
			'Brak daty oddania' :
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.status',
		text: 'Status',
		formatter: (status) => {
			switch (status) {
				case 'STARTED':
					return 'Do odbioru';
				case 'PICKED_UP':
					return 'Odebrana';
			}
		},
		sort: true
	}, {
		dataField: 'change_status',
		text: 'Zmień status',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: () => (<Button variant='info' size='sm' className='btn center-button' onClick={handleShowStatusModal}>Zmień</Button>),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedID(row.reservation.reservationID);
			}
		},
		hidden: user.role !== 'user' ? false : true
	}, {
		dataField: 'cancel_reservation',
		text: 'Anuluj rezerwację',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: (column, row) => (row.reservation.status === 'STARTED' ?
			<Button variant='danger' size='sm' className='btn center-button' onClick={handleShowCancelModal}>Anuluj</Button> :
			<Button variant='danger' size='sm' className='btn center-button' disabled>Anuluj</Button>
		),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedID(row.reservation.reservationID);
			}
		}
	}];

	const selectOptions = [
		{ value: 1, label: 'Odebrana' },
		{ value: 2, label: 'Oddana' },
		{ value: 3, label: 'Zakończona' }
	];

	const SelectComponent = () => (
		<Select options={selectOptions} isSearchable={false} placeholder='Wybierz status...' value={status === null ? '' : status} onChange={e => setStatus(e)} />
	);

	useEffect(() => {
		getReservations();
	}, []);

	return (
		<React.Fragment>
			<h2 className='mt-3'>Lista rezerwacji</h2>
			{loading ?
				<ToolkitProvider
					bootstrap4
					keyField='reservation.reservationID'
					data={reservations}
					columns={columns}
					search={{
						searchFormatted: true
					}}
				>
					{props => (
						<div>
							<div className='row'>
								<div className='ml-4 mt-2'>Wyszukaj: </div>
								<SearchBar
									{...props.searchProps}
									className='ml-2'
									placeholder='Wpisz szukaną frazę...'
								/>
								<ClearSearchButton
									{...props.searchProps}
									className='ml-2 mb-2'
									text='Wyczyść'
								/>
							</div>
							<BootstrapTable
								{...props.baseProps}
								bootstrap4
								keyField='reservation.reservationID'
								data={reservations}
								columns={columns}
								loading={true}
								striped={true}
								hover={true}
								pagination={paginationFactory({
									hideSizePerPage: true
								})}
								noDataIndication='Brak danych'
							/>
						</div>
					)}
				</ToolkitProvider>
				:
				<Spinner animation='border' />}
			<Modal show={showCancelModal} onHide={handleCloseCancelModal}>
				<Modal.Header closeButton>
					<Modal.Title>Anuluj rezerwację</Modal.Title>
				</Modal.Header>
				<Modal.Body>Czy na pewno chcesz anulować tę rezerwację?</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseCancelModal}>Wróć</Button>
					<Button variant='danger' onClick={cancelReservation}>Tak, anuluj</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={showStatusModal} onHide={handleCloseStatusModal}>
				<Modal.Header closeButton>
					<Modal.Title>Zmień status rezerwacji</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<SelectComponent />
				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseStatusModal}>Wróć</Button>
					<Button variant='info' onClick={changeStatus}>{changingStatus && <Spinner animation='border' size='sm' style={{ marginBottom: '4px' }} />} Zmień</Button>
				</Modal.Footer>
			</Modal>
		</React.Fragment>
	);
}

export default Reservations;