import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Spinner } from 'react-bootstrap';

function ReservationsHistory(props) {
	const user = props.user;
	const [reservations, setReservations] = useState([]);
	const [loading, setLoading] = useState(false);
	const { SearchBar, ClearSearchButton } = Search;

	const getReservations = async () => {
		if (user.role !== 'user') {
			try {
				await axios.get(`${process.env.REACT_APP_API_URL}api/reservation`, {
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${user.token}`
					}
				}).then(res => {
					const reservations = [];
					for (let reservation of res.data) {
						if (reservation.reservation.status === 'GAVE_BACK' || reservation.reservation.status === 'FINISHED') {
							reservations.push(reservation);
						}
					}
					setReservations(reservations);
				}).catch(error => {
					if (error.response.status === 401) {
						props.logout();
					} else {
						console.log(error);
					}
				});
				setLoading(true);
			} catch (e) {
				console.log(e);
			}
		} else {
			try {
				await axios.get(`${process.env.REACT_APP_API_URL}api/reservation/findByUser?id=${user.userID}`, {
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${user.token}`
					}
				}).then(res => {
					const reservations = [];
					for (let reservation of res.data) {
						if (reservation.reservation.status === 'GAVE_BACK' || reservation.reservation.status === 'FINISHED') {
							reservations.push(reservation);
						}
					}
					setReservations(reservations);
				}).catch(error => {
					if (error.response.status === 401) {
						props.logout();
					} else {
						console.log(error);
					}
				});
				setLoading(true);
			} catch (e) {
				console.log(e);
			}
		}
	};

	const columns = [{
		dataField: 'reservation.reservationID',
		text: 'ID',
		hidden: true
	}, {
		dataField: 'user.userID',
		text: 'Użytkownik',
		formatter: (column, row) => (`${row.user.firstname} ${row.user.surname}`),
		sort: true,
		hidden: user.role !== 'user' ? false : true
	}, {
		dataField: 'book.bookID',
		text: 'Książka',
		formatter: (column, row) => (row.book.title),
		sort: true
	}, {
		dataField: 'reservation.reservationTime',
		text: 'Data rezerwacji',
		formatter: (date) => (
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.reservationPickUpTime',
		text: 'Data odbioru',
		formatter: (date) => (date === '0001-01-01T00:00:00' ?
			'Brak daty odbioru' :
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.reservationEndTime',
		text: 'Data oddania',
		formatter: (date) => (date === '0001-01-01T00:00:00' ?
			'Brak daty oddania' :
			Intl.DateTimeFormat('pl-PL', {
				year: 'numeric',
				month: 'long',
				day: '2-digit',
				hour: 'numeric',
				minute: 'numeric'
			}).format(new Date(date))
		),
	}, {
		dataField: 'reservation.status',
		text: 'Status',
		formatter: (status) => {
			switch (status) {
				case 'GAVE_BACK':
					return 'Oddana';
				case 'FINISHED':
					return 'Zakończona';
			}
		},
		sort: true
	}];

	useEffect(() => {
		getReservations();
	}, []);

	return (
		<React.Fragment>
			<h2 className='mt-3'>Historia rezerwacji</h2>
			{loading ?
				<ToolkitProvider
					bootstrap4
					keyField='reservation.reservationID'
					data={reservations}
					columns={columns}
					search={{
						searchFormatted: true
					}}
				>
					{props => (
						<div>
							<div className='row'>
								<div className='ml-4 mt-2'>Wyszukaj: </div>
								<SearchBar
									{...props.searchProps}
									className='ml-2'
									placeholder='Wpisz szukaną frazę...'
								/>
								<ClearSearchButton
									{...props.searchProps}
									className='ml-2 mb-2'
									text='Wyczyść'
								/>
							</div>
							<BootstrapTable
								{...props.baseProps}
								bootstrap4
								keyField='reservation.reservationID'
								data={reservations}
								columns={columns}
								loading={true}
								striped={true}
								hover={true}
								pagination={paginationFactory({
									hideSizePerPage: true
								})}
								noDataIndication='Brak danych'
							/>
						</div>
					)}
				</ToolkitProvider>
				:
				<Spinner animation='border' />}
		</React.Fragment>
	);
}

export default ReservationsHistory;