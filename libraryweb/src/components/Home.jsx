import React from 'react';

const Home = () => (
	<React.Fragment>
		<center>
			<h1>Strona główna</h1>
			<h3>Witaj na stronie biblioteki RR!</h3>
			<p><a href="/login" className='text-info'>Zaloguj się</a> lub <a href="/register" className='text-info'>załóż konto</a>, aby skorzystać z zasobów.</p>
		</center>
	</React.Fragment>
);

export default Home;