import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Select from 'react-select';
import { Redirect, useParams, Link } from 'react-router-dom';
import { Button, Form } from "react-bootstrap";

const saltedMd5 = require('salted-md5');

function EditWorker(props) {
	let { id } = useParams();
	const user = props.user;
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [email, setEmail] = useState('');
	const [oldEmail, setOldEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [role, setRole] = useState(null);

	const [firstnameError, setFirstnameError] = useState(null);
	const [lastnameError, setLastnameError] = useState(null);
	const [emailError, setEmailError] = useState(null);
	const [passwordError, setPasswordError] = useState(null);
	const [password2Error, setPassword2Error] = useState(null);

	const [redirect, setRedirect] = useState(false);

	const onSubmit = (e) => {
		e.preventDefault();
		if (firstname === '') {
			setFirstnameError('Imię nie może być puste.');
		} else {
			setFirstnameError(null);
		}
		if (lastname === '') {
			setLastnameError('Nazwisko nie może być puste.');
		} else {
			setLastnameError(null);
		}
		if (email === oldEmail) {
			setEmailError(null);
		} else if (email === '') {
			setEmailError('Email nie może być pusty.');
		} else if (email.length < 6) {
			setEmailError('Email musi zawierać min. 6 znaków.');
		} else if (!/^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
			setEmailError('Proszę podać poprawny adres email.');
		} else {
			setEmailError(null);
		}
		if (password === '') {
			setPasswordError(null);
		} else if (password.length < 6) {
			setPasswordError('Hasło musi zawierać min. 6 znaków.');
		} else if (!/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*+=_-]).*$/.test(password)) {
			setPasswordError('Hasło musi zawierać min. 1 małą literę, wielką literę, liczbę oraz symbol.');
		} else if (password.toLowerCase() !== password2.toLowerCase()) {
			setPasswordError('Hasła muszą być takie same.');
			setPassword2Error('Hasła muszą być takie same.');
		} else {
			setPasswordError(null);
			setPassword2Error(null);
		}
		if (firstnameError === null && lastnameError === null && emailError === null && passwordError === null && password2 === null) {
			updateWorker();
		}
	};

	const updateWorker = async () => {
		let hashPass = null;
		if (password !== '') {
			hashPass = await saltedMd5(password, process.env.REACT_APP_PASSWORD_HASH, true);
		}
		try {
			await axios.put(`${process.env.REACT_APP_API_URL}api/user?id=${id}`, {
				userID: parseInt(id),
				firstname: firstname,
				surname: lastname,
				login: email,
				password: hashPass || null,
				role: role?.value
			}, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(() => {
				setRedirect(true);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
		} catch (e) {
			console.log(e);
		}
	};

	const selectOptions = [
		{ value: 'user', label: 'Użytkownik' },
		{ value: 'employee', label: 'Pracownik' },
		{ value: 'admin', label: 'Administrator' }
	];

	const SelectComponent = () => (
		<Select options={selectOptions} isSearchable={false} value={role === null ? selectOptions[1] : role} onChange={e => setRole(e)} />
	);

	useEffect(() => {
		try {
			axios.get(`${process.env.REACT_APP_API_URL}api/user/findById?id=${id}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(res => {
				const u = res.data;
				setFirstname(u.firstname);
				setLastname(u.surname);
				setEmail(u.login);
				setOldEmail(u.login);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
		} catch (e) {
			console.log(e);
		}
	}, []);

	return (
		<>
			<Form className='w-100 mt-2 mx-auto text-center'>
				<h1 className='mb-3'>Edycja pracownika</h1>
				<Form.Group controlId='firstname' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Imię</Form.Label>
					<Form.Control type='text' placeholder='Imię' className='form-control' value={firstname} onInput={e => setFirstname(e.target.value.trim())} />
					{firstnameError && <Form.Text className='text-danger'>{firstnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='lastname' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Nazwisko</Form.Label>
					<Form.Control type='text' placeholder='Nazwisko' className='form-control' value={lastname} onInput={e => setLastname(e.target.value.trim())} />
					{lastnameError && <Form.Text className='text-danger'>{lastnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='email' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Adres email</Form.Label>
					<Form.Control type='email' placeholder='Adres email' className='form-control' value={email} disabled />
					{emailError && <Form.Text className='text-danger'>{emailError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password} onInput={e => setPassword(e.target.value.trim())} />
					{passwordError && <Form.Text className='text-danger'>{passwordError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password2' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Powtórz hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password2} onInput={e => setPassword2(e.target.value.trim())} />
					{password2Error && <Form.Text className='text-danger'>{password2Error}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='role' className='w-25 mx-auto text-center'>
					<Form.Label className='mb-0'>Rola</Form.Label>
					<SelectComponent />
				</Form.Group>
				<Link to='/workers' className='btn btn-secondary center-button mr-3'>Powrót</Link>
				<Button type='submit' variant='success' onClick={e => onSubmit(e)}>Zmień dane</Button>
			</Form>
			{redirect && <Redirect to='/workers' />}
		</>
	);
}

export default EditWorker;