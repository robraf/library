import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Button, Modal, Spinner } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

function Workers(props) {
	const user = props.user;
	const [workers, setWorkers] = useState([]);
	const [loading, setLoading] = useState(false);
	const [clickedWorker, setClickedWorker] = useState(null);
	const [clickedID, setClickedID] = useState(null);
	const [showModal, setShowModal] = useState(false);
	const { SearchBar, ClearSearchButton } = Search;

	const handleShowModal = () => setShowModal(true);
	const handleCloseModal = () => setShowModal(false);
	const history = useHistory();

	const deleteWorker = async () => {
		try {
			await axios.delete(`${process.env.REACT_APP_API_URL}api/user?id=${clickedID}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			handleCloseModal();
		} catch (e) {
			console.log(e);
		}
		history.push('/reload');
		history.goBack();
	};

	const getWorkers = async () => {
		try {
			await axios.get(`${process.env.REACT_APP_API_URL}api/user/findByRole?role=employee`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(res => {
				setWorkers(res.data);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			setLoading(true);
		} catch (e) {
			console.log(e);
		}
	};

	const columns = [{
		dataField: 'userID',
		text: 'ID',
		hidden: true
	}, {
		dataField: 'firstname',
		text: 'Imię',
		sort: true
	}, {
		dataField: 'surname',
		text: 'Nazwisko',
		sort: true
	}, {
		dataField: 'login',
		text: 'Email',
		sort: true
	}, {
		dataField: 'edit_worker',
		text: 'Edytuj pracownika',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: (column, row) => (<Link to={`workers/edit/${row.userID}`} className='btn btn-info btn-sm center-button'>Edytuj</Link>)
	}, {
		dataField: 'delete_worker',
		text: 'Usuń pracownika',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: () => (<Button variant='danger' size='sm' className='btn center-button' onClick={handleShowModal}>Usuń</Button>),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedWorker(`${row.firstname} ${row.surname}`);
				setClickedID(row.userID);
			}
		}
	}];

	useEffect(() => {
		getWorkers();
	}, []);

	return (
		<React.Fragment>
			<h2 className='mt-3'>Lista pracowników</h2>
			{loading ?
				<ToolkitProvider
					bootstrap4
					keyField='userID'
					data={workers}
					columns={columns}
					search
				>
					{props => (
						<div>
							<div className='row'>
								<div className='ml-4 mt-2'>Wyszukaj: </div>
								<SearchBar
									{...props.searchProps}
									className='ml-2'
									placeholder='Wpisz szukaną frazę...'
								/>
								<ClearSearchButton
									{...props.searchProps}
									className='ml-2 mb-2'
									text='Wyczyść'
								/>
							</div>
							<BootstrapTable
								{...props.baseProps}
								bootstrap4
								keyField='userID'
								data={workers}
								columns={columns}
								loading={true}
								striped={true}
								hover={true}
								pagination={paginationFactory({
									hideSizePerPage: true
								})}
								noDataIndication='Brak danych'
							/>
						</div>
					)}
				</ToolkitProvider>
				:
				<Spinner animation='border' />}
			<Modal show={showModal} onHide={handleCloseModal}>
				<Modal.Header closeButton>
					<Modal.Title>Usuń pracownika</Modal.Title>
				</Modal.Header>
				<Modal.Body>Czy na pewno chcesz usunąć pracownika {clickedWorker}?</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseModal}>Wróć</Button>
					<Button variant='danger' onClick={deleteWorker}>Usuń</Button>
				</Modal.Footer>
			</Modal>
		</React.Fragment>
	);
}

export default Workers;