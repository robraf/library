import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Button, Modal, Spinner } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

function Users(props) {
	const user = props.user;
	const [users, setUsers] = useState([]);
	const [loading, setLoading] = useState(false);
	const [clickedUser, setClickedUser] = useState(null);
	const [clickedID, setClickedID] = useState(null);
	const [showModal, setShowModal] = useState(false);
	const { SearchBar, ClearSearchButton } = Search;

	const handleShowModal = () => setShowModal(true);
	const handleCloseModal = () => setShowModal(false);
	const history = useHistory();

	const deleteUser = async () => {
		try {
			await axios.delete(`${process.env.REACT_APP_API_URL}api/user?id=${clickedID}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			handleCloseModal();
		} catch (e) {
			console.log(e);
		}
		history.push('/reload');
		history.goBack();
	};

	const getUsers = async () => {
		try {
			await axios.get(`${process.env.REACT_APP_API_URL}api/user/findByRole?role=user`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(res => {
				setUsers(res.data);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			setLoading(true);
		} catch (e) {
			console.log(e);
		}
	};

	const columns = [{
		dataField: 'userID',
		text: 'ID',
		hidden: true
	}, {
		dataField: 'firstname',
		text: 'Imię',
		sort: true
	}, {
		dataField: 'surname',
		text: 'Nazwisko',
		sort: true
	}, {
		dataField: 'login',
		text: 'Email',
		sort: true
	}, {
		dataField: 'edit_user',
		text: 'Edytuj użytkownika',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: (column, row) => (<Link to={`users/edit/${row.userID}`} className='btn btn-info btn-sm center-button'>Edytuj</Link >)
	}, {
		dataField: 'delete_user',
		text: 'Usuń użytkownika',
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: () => (<Button variant='danger' size='sm' className='btn center-button' onClick={handleShowModal}>Usuń</Button>),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedUser(`${row.firstname} ${row.surname}`);
				setClickedID(row.userID);
			}
		}
	}];

	useEffect(() => {
		getUsers();
	}, []);

	return (
		<React.Fragment>
			<h2 className='mt-3'>Lista użytkowników</h2>
			{loading ?
				<ToolkitProvider
					bootstrap4
					keyField='userID'
					data={users}
					columns={columns}
					search
				>
					{props => (
						<div>
							<div className='row'>
								<div className='ml-4 mt-2'>Wyszukaj: </div>
								<SearchBar
									{...props.searchProps}
									className='ml-2'
									placeholder='Wpisz szukaną frazę...'
								/>
								<ClearSearchButton
									{...props.searchProps}
									className='ml-2 mb-2'
									text='Wyczyść'
								/>
							</div>
							<BootstrapTable
								{...props.baseProps}
								bootstrap4
								keyField='userID'
								data={users}
								columns={columns}
								loading={true}
								striped={true}
								hover={true}
								pagination={paginationFactory({
									hideSizePerPage: true
								})}
								noDataIndication='Brak danych'
							/>
						</div>
					)}
				</ToolkitProvider>
				:
				<Spinner animation='border' />}
			<Modal show={showModal} onHide={handleCloseModal}>
				<Modal.Header closeButton>
					<Modal.Title>Usuń użytkownika</Modal.Title>
				</Modal.Header>
				<Modal.Body>Czy na pewno chcesz usunąć użytkownika {clickedUser}?</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseModal}>Wróć</Button>
					<Button variant='danger' onClick={deleteUser}>Usuń</Button>
				</Modal.Footer>
			</Modal>
		</React.Fragment>
	);
}

export default Users;