import React, { useState } from 'react';
import axios from 'axios';
import { Button, Form, Modal } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';

const saltedMd5 = require('salted-md5');

function NewUser(props) {
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');

	const [firstnameError, setFirstnameError] = useState(null);
	const [lastnameError, setLastnameError] = useState(null);
	const [emailError, setEmailError] = useState(null);
	const [passwordError, setPasswordError] = useState(null);
	const [password2Error, setPassword2Error] = useState(null);

	const [show, setShow] = useState(false);
	const handleOpenModal = () => setShow(true);
	const handleCloseModal = () => {
		setShow(false);
		history.go('/books');
	};
	const history = useHistory();

	const onSubmit = (e) => {
		handleOpenModal();
		e.preventDefault();
		var noErrors = true;
		if (firstname === '') {
			setFirstnameError('Imię nie może być puste.');
			noErrors = false;
		} else {
			setFirstnameError(null);
		}
		if (lastname === '') {
			setLastnameError('Nazwisko nie może być puste.');
			noErrors = false;
		} else {
			setLastnameError(null);
		}
		if (email === '') {
			setEmailError('Email nie może być pusty.');
			noErrors = false;
		} else if (email.length < 6) {
			setEmailError('Email musi zawierać min. 6 znaków.');
			noErrors = false;
		} else if (!/^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
			setEmailError('Proszę podać poprawny adres email.');
			noErrors = false;
		} else {
			setEmailError(null);
		}
		if (password2 === '') {
			setPassword2Error('Hasło nie może być puste.');
			noErrors = false;
		}
		if (password === '') {
			setPasswordError('Hasło nie może być puste.');
			noErrors = false;
		} else if (password.length < 6) {
			setPasswordError('Hasło musi zawierać min. 6 znaków.');
			noErrors = false;
		} else if (!/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*+=_-]).*$/.test(password)) {
			setPasswordError('Hasło musi zawierać min. 1 małą literę, wielką literę, liczbę oraz symbol.');
			noErrors = false;
		} else if (password.toLowerCase() !== password2.toLowerCase()) {
			setPasswordError('Hasła muszą być takie same.');
			setPassword2Error('Hasła muszą być takie same.');
			noErrors = false;
		} else {
			setPasswordError(null);
			setPassword2Error(null);
		}
		if (noErrors) {
			createUser();
		}
	};

	const createUser = async () => {
		const hashPass = await saltedMd5(password, process.env.REACT_APP_PASSWORD_HASH, true);
		await axios.post(`${process.env.REACT_APP_API_URL}api/user`, {
			headers: {
				'Content-Type': 'application/json'
			},
			firstname: firstname,
			surname: lastname,
			login: email,
			password: hashPass
		}).then(() => {
			handleOpenModal();
		}).catch(error => {
			if (error.response.status === 400) {
				console.log(error.response);
			} else if (error.response.status === 401) {
				props.logout();
			} else {
				console.log(error);
			}
		});
	};

	return (
		<>
			<Form className='w-50 mt-2 mx-auto text-center'>
				<h1 className='mb-3'>Nowy użytkownik</h1>
				<Form.Group controlId='firstname' className='w-50 mx-auto'>
					<Form.Label className='mb-0'>Imię</Form.Label>
					<Form.Control type='text' placeholder='Imię' className='form-control' value={firstname} onInput={e => setFirstname(e.target.value)} />
					{firstnameError && <Form.Text className='text-danger'>{firstnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='lastname' className='w-50 mx-auto'>
					<Form.Label className='mb-0'>Nazwisko</Form.Label>
					<Form.Control type='text' placeholder='Nazwisko' className='form-control' value={lastname} onInput={e => setLastname(e.target.value)} />
					{lastnameError && <Form.Text className='text-danger'>{lastnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='email' className='w-50 mx-auto'>
					<Form.Label className='mb-0'>Adres email</Form.Label>
					<Form.Control type='email' placeholder='Adres email' className='form-control' value={email} onInput={e => setEmail(e.target.value)} />
					{emailError && <Form.Text className='text-danger'>{emailError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password' className='w-50 mx-auto'>
					<Form.Label className='mb-0'>Hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password} onInput={e => setPassword(e.target.value)} />
					{passwordError && <Form.Text className='text-danger'>{passwordError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password2' className='w-50 mx-auto'>
					<Form.Label className='mb-0'>Powtórz hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password2} onInput={e => setPassword2(e.target.value)} />
					{password2Error && <Form.Text className='text-danger'>{password2Error}</Form.Text>}
				</Form.Group>
				<Button type='submit' variant='success' onClick={e => onSubmit(e)}>Utwórz</Button>
			</Form>
			<Modal show={show} onHide={handleCloseModal}>
				<Modal.Header closeButton>
					<Modal.Title>Utworzono użytkownika!</Modal.Title>
				</Modal.Header>
				<Modal.Footer>
					<Button variant='secondary' className='mx-auto' onClick={handleCloseModal}>Zamknij</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
}

export default NewUser;