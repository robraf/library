import React from "react";
import { Redirect, Route, Switch } from 'react-router-dom';

import Home from './Home';
import NotFound from './NotFound';
import Login from './authentication/Login';
import Register from './authentication/Register';
import Logout from './authentication/Logout';
import Books from './books/Books';
import AddBook from './books/AddBook';
import EditBook from './books/EditBook';
import Reservations from './reservations/Reservations';
import ReservationsHistory from './reservations/ReservationsHistory';
import Users from './admin/users/Users';
import EditUser from './admin/users/EditUser';
import Workers from './admin/workers/Workers';
import EditWorker from './admin/workers/EditWorker';
import NewUser from './admin/users/NewUser';
import Settings from './Settings';

function Router(props) {
	const user = props.user;
	const emailError = props.emailError;
	const passwordError = props.passwordError;
	const redirectToLogin = props.redirectToLogin;

	const login = (email, password) => {
		props.login(email, password);
	};

	const register = (firstname, lastname, email, password, password2) => {
		props.register(firstname, lastname, email, password, password2);
	};

	const logout = () => {
		props.logout();
	};

	return (
		<Switch>
			<Route exact path='/'>
				{user ? <Redirect to='/books' /> : <Home />}
			</Route>
			<Route exact path='/login'>
				{user ? <Books user={user} logout={logout} /> : <Login login={login} emailError={emailError} passwordError={passwordError} />}
			</Route>
			<Route exact path='/register'>
				{user ? <Books user={user} logout={logout} /> : <Register register={register} emailError={emailError} redirectToLogin={redirectToLogin} />}
			</Route>
			<Route exact path='/logout'>
				{user ? <Logout logout={logout} /> : <Redirect to='/login' />}
			</Route>
			<Route exact path='/books'>
				{user ? <Books user={user} logout={logout} /> : <Redirect to='/login' />}
			</Route>
			<Route exact path='/books/add'>
				{user ?
					user?.role !== 'user' ?
						<AddBook user={user} logout={logout} />
						:
						<Redirect to='/books' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route exact path='/books/edit/:id'>
				{user ?
					user?.role !== 'user' ?
						<EditBook user={user} logout={logout} />
						:
						<Redirect to='/books' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route exact path='/reservations'>
				{user ? <Reservations user={user} logout={logout} /> : <Redirect to='/login' />}
			</Route>
			<Route exact path='/reservations/history'>
				{user ? <ReservationsHistory user={user} logout={logout} /> : <Redirect to='/login' />}
			</Route>
			<Route exact path='/users'>
				{user ?
					user?.role === 'admin' ?
						<Users user={user} logout={logout} />
						:
						<Redirect to='/books' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route path='/users/edit/:id'>
				{user ?
					user?.role === 'admin' ?
						<EditUser user={user} logout={logout} />
						:
						<Redirect to='/users' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route exact path='/workers'>
				{user ?
					user?.role === 'admin' ?
						<Workers user={user} logout={logout} />
						:
						<Redirect to='/books' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route path='/workers/edit/:id'>
				{user ?
					user?.role === 'admin' ?
						<EditWorker user={user} logout={logout} />
						:
						<Redirect to='/workers' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route path='/newuser'>
				{user ?
					user?.role !== 'user' ?
						<NewUser user={user} logout={logout} />
						:
						<Redirect to='/books' />
					:
					<Redirect to='/login' />}
			</Route>
			<Route exact path='/settings'>
				{user ? <Settings user={user} logout={logout} /> : <Redirect to='/login' />}
			</Route>
			<Route path='/'>
				{user ? <NotFound /> : <Redirect to='/login' />}
			</Route>
		</Switch>
	);
}

export default Router;