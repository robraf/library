import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Button, Modal, Row, Col, Spinner } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

function Books(props) {
	const user = props.user;
	const [books, setBooks] = useState([]);
	const [loading, setLoading] = useState(false);
	const [clickedBook, setClickedBook] = useState(null);
	const [clickedID, setClickedID] = useState(null);
	const [deleteModal, setDeleteModal] = useState(false);
	const [successModal, setSuccessModal] = useState(false);
	const [failModal, setFailModal] = useState(false);
	const [reservationModal, setReservationModal] = useState(false);
	const [makingReservation, setMakingReservation] = useState(false);
	const { SearchBar, ClearSearchButton } = Search;

	const handleDeleteModal = () => setDeleteModal(true);
	const handleCloseDeleteModal = () => setDeleteModal(false);
	const handleReservationModal = () => setReservationModal(true);
	const handleCloseReservationModal = () => setReservationModal(false);
	const handleSuccessModal = () => setSuccessModal(true);
	const handleCloseSuccessModal = () => {
		setSuccessModal(false);
		history.push('/reload');
		history.goBack();
	};
	const handleFailModal = () => setFailModal(true);
	const handleCloseFailModal = () => setFailModal(false);

	const history = useHistory();

	const deleteBook = async () => {
		try {
			await axios.delete(`${process.env.REACT_APP_API_URL}api/book?id=${clickedID}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			handleCloseDeleteModal();
		} catch (e) {
			console.log(e);
		}
		history.push('/reload');
		history.goBack();
	};

	const makeReservation = async () => {
		try {
			setMakingReservation(true);
			await axios.post(`${process.env.REACT_APP_API_URL}api/reservation`, {
				userID: parseInt(user.userID),
				bookID: parseInt(clickedID)
			}, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(() => {
				setMakingReservation(false);
				handleCloseReservationModal();
				handleSuccessModal();
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					setMakingReservation(false);
					handleCloseReservationModal();
					handleFailModal();
					console.log(error);
				}
			});
		} catch (e) {
			console.log(e);
		}
	};

	const getBooks = async () => {
		try {
			await axios.get(`${process.env.REACT_APP_API_URL}api/book`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(res => {
				setBooks(res.data);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
			setLoading(true);
		} catch (e) {
			console.log(e);
		}
	};

	const columns = [{
		dataField: 'bookID',
		text: 'ID',
		hidden: true
	}, {
		dataField: 'title',
		text: 'Tytuł',
		sort: true
	}, {
		dataField: 'author',
		text: 'Autor',
		sort: true
	}, {
		dataField: 'publisher',
		text: 'Wydawnictwo',
		sort: true
	}, {
		dataField: 'publishedYear',
		text: 'Rok wydania',
		sort: true
	}, {
		dataField: 'description',
		text: 'Szczegóły'
	}, {
		dataField: 'category',
		text: 'Kategoria',
		sort: true
	}, {
		dataField: 'language',
		text: 'Język',
		sort: true
	}, {
		dataField: 'location',
		text: 'Lokalizacja'
	}, {
		dataField: 'reservation',
		text: 'Rezerwacja',
		hidden: user.role === 'user' ? false : true,
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: (column, row) => (row.reservationID === null ?
			<Button variant='success' size='sm' className='btn center-button' onClick={handleReservationModal}>Rezerwuj</Button> :
			<Button variant='danger' size='sm' className='btn center-button' disabled>Rezerwuj</Button>
		),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedBook(`"${row.title}"`);
				setClickedID(row.bookID);
			}
		}
	}, {
		dataField: 'edit_book',
		text: 'Edytuj książkę',
		hidden: user.role !== 'user' ? false : true,
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: (column, row) => (<Link to={`books/edit/${row.bookID}`} className='btn btn-info btn-sm center-button'>Edytuj</Link >)
	}, {
		dataField: 'delete_book',
		text: 'Usuń książkę',
		hidden: user.role !== 'user' ? false : true,
		headerFormatter: (column) => (<div className='text-center'>{column.text}</div>),
		formatter: () => (<Button variant='danger' size='sm' className='btn center-button' onClick={handleDeleteModal}>Usuń</Button>),
		events: {
			onClick: (e, col, colIndex, row, rowIndex) => {
				setClickedBook(`"${row.title}"`);
				setClickedID(row.bookID);
			}
		}
	}];

	useEffect(() => {
		getBooks();
	}, []);

	return (
		<React.Fragment>
			<h2 className='mt-3'>Lista książek</h2>
			{loading ?
				<ToolkitProvider
					bootstrap4
					keyField='bookID'
					data={books}
					columns={columns}
					search
				>
					{props => (
						<div>
							<Row>
								<Col>
									<div className='ml-4 mt-2'>Wyszukaj: </div>
									<SearchBar
										{...props.searchProps}
										className='ml-2'
										placeholder='Wpisz szukaną frazę...'
									/>
									<ClearSearchButton
										{...props.searchProps}
										className='ml-2 mb-2'
										text='Wyczyść'
									/>
								</Col>
								<Col>
									<Link to='/books/add' className='btn btn-success mt-3 mr-4 float-right'>Dodaj książkę</Link>
								</Col>
							</Row>
							<BootstrapTable
								{...props.baseProps}
								bootstrap4
								keyField='bookID'
								data={books}
								columns={columns}
								loading={true}
								striped={true}
								hover={true}
								pagination={paginationFactory({
									hideSizePerPage: true
								})}
								noDataIndication='Brak danych'
							/>
						</div>
					)}
				</ToolkitProvider>
				:
				<Spinner animation='border' />}
			<Modal show={deleteModal} onHide={handleCloseDeleteModal}>
				<Modal.Header closeButton>
					<Modal.Title>Usuń książkę</Modal.Title>
				</Modal.Header>
				<Modal.Body>Czy na pewno chcesz usunąć książkę {clickedBook}?</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseDeleteModal}>Wróć</Button>
					<Button variant='danger' onClick={deleteBook}>Usuń</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={reservationModal} onHide={handleCloseReservationModal}>
				<Modal.Header closeButton>
					<Modal.Title>Rezerwuj książkę</Modal.Title>
				</Modal.Header>
				<Modal.Body>Czy na pewno chcesz zarezerwować książkę {clickedBook}?</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseReservationModal}>Wróć</Button>
					<Button variant='success' onClick={makeReservation}>{makingReservation && <Spinner animation='border' size='sm' style={{ marginBottom: '4px' }} />} Rezerwuj</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={successModal} onHide={handleCloseSuccessModal}>
				<Modal.Header closeButton>
					<Modal.Title>Zarezerwowano!</Modal.Title>
				</Modal.Header>
				<Modal.Body>Pomyślnie zarezerwowano książkę {clickedBook}. Czeka na odbiór przez 3 dni.</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseSuccessModal}>Zamknij</Button>
				</Modal.Footer>
			</Modal>
			<Modal show={failModal} onHide={handleCloseFailModal}>
				<Modal.Header closeButton>
					<Modal.Title>Zarezerwowano!</Modal.Title>
				</Modal.Header>
				<Modal.Body></Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={handleCloseFailModal}>Zamknij</Button>
				</Modal.Footer>
			</Modal>
		</React.Fragment>
	);
}

export default Books;