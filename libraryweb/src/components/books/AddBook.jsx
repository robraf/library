import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Form } from 'react-bootstrap';
import { Redirect, Link } from 'react-router-dom';

function Register(props) {
	const user = props.user;
	const [title, setTitle] = useState('');
	const [author, setAuthor] = useState('');
	const [publisher, setPublisher] = useState('');
	const [year, setYear] = useState('');
	const [description, setDescription] = useState('');
	const [category, setCategory] = useState('');
	const [language, setLanguage] = useState('');
	const [location, setLocation] = useState('');

	const [titleError, setTitleError] = useState(null);
	const [authorError, setAuthorError] = useState(null);
	const [publisherError, setPublisherError] = useState(null);
	const [yearError, setYearError] = useState(null);
	const [descriptionError, setDescriptionError] = useState(null);
	const [categoryError, setCategoryError] = useState(null);
	const [languageError, setLanguageError] = useState(null);
	const [locationError, setLocationError] = useState(null);

	const [redirect, setRedirect] = useState(false);

	const onSubmit = (e) => {
		e.preventDefault();
		var noErrors = true;
		if (title === '') {
			setTitleError('Tytuł nie może być pusty.');
			noErrors = false;
		} else {
			setTitleError(null);
		}
		if (author === '') {
			setAuthorError('Autor nie może być pusty.');
			noErrors = false;
		} else {
			setAuthorError(null);
		}
		if (publisher === '') {
			setPublisherError('Wydawnictwo nie może być puste.');
			noErrors = false;
		} else {
			setPublisherError(null);
		}
		if (year === '') {
			setYearError('Rok wydania nie może być pusty.');
			noErrors = false;
		} else if (year < 1900 || year > new Date().getFullYear()) {
			setYearError('Proszę podać poprawny rok wydania.');
			noErrors = false;
		} else {
			setYearError(null);
		}
		if (description === '') {
			setDescriptionError('Opis nie może być pusty.');
			noErrors = false;
		} else {
			setDescriptionError(null);
		}
		if (category === '') {
			setCategoryError('Kategoria nie może być pusta.');
			noErrors = false;
		} else {
			setCategoryError(null);
		}
		if (language === '') {
			setLanguageError('Język nie może być pusty.');
			noErrors = false;
		} else {
			setLanguageError(null);
		}
		if (location === '') {
			setLocationError('Lokalizacja nie może być pusta.');
			noErrors = false;
		} else {
			setLocationError(null);
		}
		if (noErrors) {
			addBook();
		}
	};

	const addBook = async () => {
		try {
			await axios.post(`${process.env.REACT_APP_API_URL}api/book`, {
				title,
				author,
				publisher,
				publishedYear: parseInt(year),
				description,
				category,
				language,
				location
			}, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${user.token}`
				}
			}).then(() => {
				setRedirect(true);
			}).catch(error => {
				if (error.response.status === 401) {
					props.logout();
				} else {
					console.log(error);
				}
			});
		} catch (e) {
			console.log(e);
		}
	};

	return (
		<>
			<Form className='w-25 mt-2 mx-auto text-center'>
				<h1 className='mb-3'>Dodaj książkę</h1>
				<Form.Group controlId='title'>
					<Form.Label className='mb-0'>Tytuł</Form.Label>
					<Form.Control type='text' placeholder='Tytuł' className='form-control' value={title} onInput={e => setTitle(e.target.value)} />
					{titleError && <Form.Text className='text-danger'>{titleError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='author'>
					<Form.Label className='mb-0'>Autor</Form.Label>
					<Form.Control type='text' placeholder='Autor' className='form-control' value={author} onInput={e => setAuthor(e.target.value)} />
					{authorError && <Form.Text className='text-danger'>{authorError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='publisher'>
					<Form.Label className='mb-0'>Wydawnictwo</Form.Label>
					<Form.Control type='text' placeholder='Wydawnictwo' className='form-control' value={publisher} onInput={e => setPublisher(e.target.value)} />
					{publisherError && <Form.Text className='text-danger'>{publisherError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='year'>
					<Form.Label className='mb-0'>Rok wydania</Form.Label>
					<Form.Control type='number' min={1900} placeholder='Rok wydania' value={year} onInput={e => setYear(e.target.value)} />
					{yearError && <Form.Text className='text-danger'>{yearError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='description'>
					<Form.Label className='mb-0'>Szczegóły</Form.Label>
					<Form.Control type='text' placeholder='Szczegóły' value={description} onInput={e => setDescription(e.target.value)} />
					{descriptionError && <Form.Text className='text-danger'>{descriptionError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='category'>
					<Form.Label className='mb-0'>Kategoria</Form.Label>
					<Form.Control type='text' placeholder='Kategoria' value={category} onInput={e => setCategory(e.target.value)} />
					{categoryError && <Form.Text className='text-danger'>{categoryError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='language'>
					<Form.Label className='mb-0'>Język</Form.Label>
					<Form.Control type='text' placeholder='Język' value={language} onInput={e => setLanguage(e.target.value)} />
					{languageError && <Form.Text className='text-danger'>{languageError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='location'>
					<Form.Label className='mb-0'>Lokalizacja</Form.Label>
					<Form.Control type='text' placeholder='Lokalizacja' value={location} onInput={e => setLocation(e.target.value)} />
					{locationError && <Form.Text className='text-danger'>{locationError}</Form.Text>}
				</Form.Group>
				<Link to='/books' className='btn btn-secondary center-button mr-3 mb-5'>Powrót</Link>
				<Button type='submit' variant='success' className='mb-5' onClick={e => onSubmit(e)}>Dodaj</Button>
			</Form>
			{redirect && <Redirect to='/books' />}
		</>
	);
};

export default Register;