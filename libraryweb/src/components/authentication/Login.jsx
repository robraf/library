import React, { useState, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';

function Login(props) {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [emailError, setEmailError] = useState(null);
	const [passwordError, setPasswordError] = useState(null);

	const onSubmit = async (e) => {
		e.preventDefault();
		if (email === '') {
			setEmailError('Email nie może być pusty.');
		} else {
			setEmailError(null);
		}
		if (password === '') {
			setPasswordError('Hasło nie może być puste.');
		} else {
			setPasswordError(null);
		}
		props.login(email, password);
	};

	useEffect(() => {
		if (props.emailError !== null) setEmailError(props.emailError);
		if (props.passwordError !== null) setPasswordError(props.passwordError);
	}, [props]);

	return (
		<Form className='w-25 mt-2 mx-auto text-center'>
			<h1 className='mb-3'>Logowanie</h1>
			<Form.Group controlId='email'>
				<Form.Label className='mb-0'>Adres email</Form.Label>
				<Form.Control type='email' placeholder='Adres email' className='form-control' value={email} onInput={e => setEmail(e.target.value)} />
				{emailError && <Form.Text className='text-danger'>{emailError}</Form.Text>}
			</Form.Group>
			<Form.Group controlId='password'>
				<Form.Label className='mb-0'>Hasło</Form.Label>
				<Form.Control type='password' placeholder='Hasło' value={password} onInput={e => setPassword(e.target.value)} />
				{passwordError && <Form.Text className='text-danger'>{passwordError}</Form.Text>}
			</Form.Group>
			<Form.Group controlId='remember'>
				<Form.Check type='checkbox' label='Zapamiętaj mnie' />
			</Form.Group>
			<Button type='submit' variant='success' onClick={e => onSubmit(e)}>Zaloguj</Button>
		</Form>
	);
}

export default Login;