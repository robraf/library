import React, { useState, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

function Register(props) {
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');

	const [firstnameError, setFirstnameError] = useState(null);
	const [lastnameError, setLastnameError] = useState(null);
	const [emailError, setEmailError] = useState(null);
	const [passwordError, setPasswordError] = useState(null);
	const [password2Error, setPassword2Error] = useState(null);

	const onSubmit = (e) => {
		e.preventDefault();
		if (firstname === '') {
			setFirstnameError('Imię nie może być puste.');
		} else {
			setFirstnameError(null);
		}
		if (lastname === '') {
			setLastnameError('Nazwisko nie może być puste.');
		} else {
			setLastnameError(null);
		}
		if (email === '') {
			setEmailError('Email nie może być pusty.');
		} else if (email.length < 6) {
			setEmailError('Email musi zawierać min. 6 znaków.');
		} else if (!/^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
			setEmailError('Proszę podać poprawny adres email.');
		} else {
			setEmailError(null);
		}
		if (password2 === '') {
			setPassword2Error('Hasło nie może być puste.');
		}
		if (password === '') {
			setPasswordError('Hasło nie może być puste.');
		} else if (password.length < 6) {
			setPasswordError('Hasło musi zawierać min. 6 znaków.');
		} else if (!/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*+=_-]).*$/.test(password)) {
			setPasswordError('Hasło musi zawierać min. 1 małą literę, wielką literę, liczbę oraz symbol.');
		} else if (password.toLowerCase() !== password2.toLowerCase()) {
			setPasswordError('Hasła muszą być takie same.');
			setPassword2Error('Hasła muszą być takie same.');
		} else {
			setPasswordError(null);
			setPassword2Error(null);
		}
		props.register(firstname, lastname, email, password, password2);
	};

	useEffect(() => {
		if (props.emailError !== null) setEmailError(props.emailError);
	}, [props]);

	return (
		<>
			<Form className='w-25 mt-2 mx-auto text-center'>
				<h1 className='mb-3'>Rejestracja</h1>
				<Form.Group controlId='firstname'>
					<Form.Label className='mb-0'>Imię</Form.Label>
					<Form.Control type='text' placeholder='Imię' className='form-control' value={firstname} onInput={e => setFirstname(e.target.value)} />
					{firstnameError && <Form.Text className='text-danger'>{firstnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='lastname'>
					<Form.Label className='mb-0'>Nazwisko</Form.Label>
					<Form.Control type='text' placeholder='Nazwisko' className='form-control' value={lastname} onInput={e => setLastname(e.target.value)} />
					{lastnameError && <Form.Text className='text-danger'>{lastnameError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='email'>
					<Form.Label className='mb-0'>Adres email</Form.Label>
					<Form.Control type='email' placeholder='Adres email' className='form-control' value={email} onInput={e => setEmail(e.target.value)} />
					{emailError && <Form.Text className='text-danger'>{emailError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password'>
					<Form.Label className='mb-0'>Hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password} onInput={e => setPassword(e.target.value)} />
					{passwordError && <Form.Text className='text-danger'>{passwordError}</Form.Text>}
				</Form.Group>
				<Form.Group controlId='password2'>
					<Form.Label className='mb-0'>Powtórz hasło</Form.Label>
					<Form.Control type='password' placeholder='Hasło' value={password2} onInput={e => setPassword2(e.target.value)} />
					{password2Error && <Form.Text className='text-danger'>{password2Error}</Form.Text>}
				</Form.Group>
				<Button type='submit' variant='success' onClick={e => onSubmit(e)}>Zarejestruj</Button>
			</Form>
			{props.redirectToLogin && <Redirect to='/login' />}
		</>
	);
}

export default Register;