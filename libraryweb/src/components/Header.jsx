import React, { useState } from 'react';
import { Navbar, Nav, Collapse } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Header(props) {
	const user = props.user;

	return (
		<Navbar collapseOnSelect bg='dark' variant='dark' expand='lg'>
			<Navbar.Brand as={Link} to='/'>LIBRARY</Navbar.Brand>
			<Navbar.Toggle aria-controls='responsive-navbar-nav' />
			{user ?
				// User is logged in
				<Navbar.Collapse>
					<Nav>
						<Nav.Link eventKey='11' as={Link} to='/books'>Książki</Nav.Link>
						<NavDropdown title='Rezerwacje' id='reservations-dropdown'>
							<NavDropdown.Item eventKey='12' as={Link} to='/reservations'>Lista rezerwacji</NavDropdown.Item>
							<NavDropdown.Item eventKey='13' as={Link} to='/reservations/history'>Historia Rezerwacji</NavDropdown.Item>
						</NavDropdown>
						{user?.role === 'admin' &&
							<React.Fragment>
								<Nav.Link eventKey='51' as={Link} to='/users'>Użytkownicy</Nav.Link>
								<Nav.Link eventKey='52' as={Link} to='/workers'>Pracownicy</Nav.Link>
							</React.Fragment>}
						{user?.role !== 'user' && <Nav.Link eventKey='60' as={Link} to='/newuser'>Nowy użytkownik</Nav.Link>}
						<Nav.Link eventKey='98' as={Link} to='/settings'>Ustawienia</Nav.Link>
					</Nav>
					<Nav className='ml-auto'>
						<Nav.Link eventKey='99' as={Link} to='/logout'>Wyloguj</Nav.Link>
					</Nav>
				</Navbar.Collapse>
				:
				// User is not logged in
				<Navbar.Collapse>
					<Nav>
						<Nav.Link eventKey='1' as={Link} to='/register'>Rejestracja</Nav.Link>
						<Nav.Link eventKey='2' as={Link} to='/login'>Logowanie</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			}
		</Navbar>
	);
}

export default Header;